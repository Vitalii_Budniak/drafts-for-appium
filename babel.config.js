/*
  This config is replacement for .babelrc and
  required by babel-jest to work properly for e2e

  More info here: https://github.com/facebook/jest/issues/6053#issuecomment-383632515
*/
module.exports = {
  presets: ['@babel/preset-env'],
  plugins: [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
  ],
};
