module.exports = {
  rootDir: './jest',
  testMatch: ['**/*.e2e.spec.js'],
  testEnvironment: '<rootDir>/AppiumEnvironment.js',
  //Custom Reporter Configuration:
  globalSetup: '<rootDir>/jestGlobalSetup.js',
  setupTestFrameworkScriptFile: '<rootDir>/jestSetup.js',
  globalTeardown: '<rootDir>/jestGlobalTeardown.js'
};
