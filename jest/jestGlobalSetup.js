const path = require('path');

const CUSTOM_REPORTER_PATH = path.resolve('../reportCommons');

const { resetReportDirectory } = require(CUSTOM_REPORTER_PATH);

module.exports = async function () {
  // Delete screenshots from a previous test run
  await resetReportDirectory();
};
