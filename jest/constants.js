export const NEW_ADMIN_LOCAL_URL = 'http://localhost:5200';
export const NEW_ADMIN_BETA_URL = 'https://admin-beta.askpoly.co/';
export const SITE_LOCAL_URL = 'http://localhost:4000';
export const SITE_BETA_URL = 'https://site-beta.askpoly.co/';
export const TIAA_LOCAL_URL = 'http://localhost:5100/';
export const SECURE_IT_LOCAL_URL = 'http://localhost:5300/';
export const SECURE_IT_BETA_URL = 'https://secureit-beta.askpoly.co/';
export const CUSTOM_REPORTER_PATH_UBUNTU = "/home/vitalii/Work/Mobile/MobileJest/reportCommons.js";
