// don't use ES7 features here, because it isn't processed by Babel
const path = require('path');

const APK_PATH = path.resolve('../../Apks/poly-192-168-0-100-3000.apk');

//const APK_PATH_UBUNTU = "/home/vitalii/Work/Mobile/Apks/poly-192-168-0-100-3000.apk";
//const APK_PATH_MACOS = "/Users/mc92/Mobile/JestMobile/Apks/poly-local-192-168-0-100-3000.apk";

const NodeEnvironment = require('jest-environment-node');
const wdio = require("webdriverio");

class AppiumEnvironment extends NodeEnvironment {
    async setup() {
        const opts = {
            port: 4723,
            capabilities: {
                platformName: "Android",
                platformVersion: "9",
                //platformVersion: "10",
                //deviceName: "33dd7dc",
                deviceName: "emulator-5554",
                app: APK_PATH,
                /*app: "/home/vitalii/Work/Mobile/Apks/ApiDemos-debug.apk",
                appPackage: "io.appium.android.apis",
                appActivity: ".view.TextFields",*/
                automationName: "UiAutomator2",
            }
        };
        this.global.mobile = await wdio.remote(opts);
        await super.setup();
    }

    async teardown() {
        this.global.mobile.deleteSession();
        await super.teardown();
    }

    runScript(script) {
        return super.runScript(script);
    }
}

module.exports = AppiumEnvironment;

