import { loginMobileApp, techLead } from './Page-objects/Login.js';
import {
  elementGetText,
  elementIsDisplay,
  scrollUp,
  scrollDownToElement,
  backButton,
} from './Helpers/Functions';
import {
  jobsDescription,
  getRandomNumber,
  loremWords,
  productName,
} from './Helpers/simple-faker/simpleFaker';

import * as hoverMenus from './Page-objects/HoverMenus';
import * as project from './Page-objects/Projects';

describe('Create-Edit WO (Android)', () => {
  beforeAll(async () => {
    jest.setTimeout(80000);
    await mobile.setTimeouts(10000);
    await loginMobileApp(techLead);
  });

  let descriptionWO = `Mob Auto test ${jobsDescription()} ${getRandomNumber(
    100,
  )}`;


  it('Test 1. Login Success', async () => {
    await hoverMenus.hoverArrowMenuClick();
    await hoverMenus.myWOClick();
    expect(await elementGetText(hoverMenus.myWO)).toMatch('My Work Orders');
  });

  it('Test 2. Create WO Assigned To Me', async () => {
    await hoverMenus.polyIconClick();
    await hoverMenus.createNewWOClick();
    await project.servTypeInput('Doors');
    await project.propertyInput('Eurest Test Property');
    await project.prioritySelect('Normal');
    await project.locationInput('Ternopil');
    await project.descriptionInput(descriptionWO);
    await project.assignToMeSelect();
    await project.createWOclick();
    //while reactivity not working I have to do inefficient approach to refresh work orders list
    await backButton();
    await hoverMenus.ferfreshMyWO();
    //
    await scrollDownToElement(project.elemByText(descriptionWO));
    expect(
      await elementIsDisplay(project.elemByText(descriptionWO), 7000),
    ).toBeTruthy();
    await mobile.pause(1000);
  });

  it('Test 3. Select And Start WO Assigned To Me', async () => {
    await project.clickElemByText(descriptionWO);
    //await project.firstActiveProjectClick();
    await project.startWOclick();
    expect(await elementIsDisplay(project.statusInProcess, 5000)).toBeTruthy();
    await mobile.pause(1000);
  });

  it('Test 4. Add WO Update', async () => {
    const texUpdate = loremWords();
    await project.updatesEnter(texUpdate);
    expect(
      await elementIsDisplay(project.elemByText(texUpdate), 5000),
    ).toBeTruthy();
  });

  it('Test 5. Add WO Time', async function() {
    await backButton();
    await project.timeEnter();
    expect(
      await elementIsDisplay(project.timeCheck('1.25'), 2000),
    ).toBeTruthy();
  });

  it('Test 6. Add Materials', async () => {
    const materialDescr = productName();
    const quantity = 7;
    const price = 7.77;
    await backButton();
    await project.addMaterials();
    await project.addMeterialsData(materialDescr, quantity, price);
    expect(
      await elementIsDisplay(project.totalCheck(quantity * price), 2000),
    ).toBeTruthy();

    await project.clickElemByText('ADD MATERIAL');
    expect(await elementIsDisplay(project.materialsRowCheck(materialDescr, `$${price} x ${quantity}`, `$${quantity * price}`), 3000)).toBeTruthy();
  });

  it('Test 7. Assets', async () => {
    const assetNumb = 'E-PIU-527';
    //const assetNumb = 'B-ECH-14';
    await backButton();
    await project.assetsAdd(assetNumb);
    expect(
      await elementIsDisplay(project.assetRowCheck(assetNumb), 3000),
    ).toBeTruthy();
  });

  it('Test 8. On Hold WO', async () => {
    const texUpdate = loremWords();
    await backButton();
    await project.statusFromTo('IN PROCESS', 'ON HOLD');
    await project.onHoldFromTo('On hold - Parts', 'On hold - Client');
    await project.statusUpdateText(texUpdate);
    await project.holdPress();
    await scrollUp();
    expect(
      await elementIsDisplay(project.elemByText('ON HOLD CLIENT'), 3000),
    ).toBeTruthy();
  });

  it('Test 9. In Pocess WO', async () => {
    const texUpdate = 'Do Active WO again From Mobile Auto Test';
    await backButton();
    await hoverMenus.hoverArrowMenuClick();
    await hoverMenus.onHoldClick();
    await project.clickElemByText('ON HOLD CLIENT');
    await project.statusFromTo('ON HOLD', 'IN PROCESS');
    await project.statusUpdateText(texUpdate);
    await project.inProcessPress();
    expect(
      await elementIsDisplay(project.woStatus('IN PROCESS'), 3000),
    ).toBeTruthy();
  });

  it('Test 10. Complete WO', async () => {
    const texUpdate = 'Complete WO From Mobile Auto Test';
    await project.statusFromTo('IN PROCESS', 'COMPLETED');
    await project.statusUpdateText(texUpdate);
    await project.completedPress();
    expect(
      await elementIsDisplay(project.woStatus('COMPLETED'), 3000),
    ).toBeTruthy();
    await backButton();
  });


  it('Test 11. Create WO assigned to Queue', async () => {
    descriptionWO = `Mob Auto test ${jobsDescription()} ${getRandomNumber(
      100,
    )}`;
    await hoverMenus.polyIconClick();
    await hoverMenus.createNewWOClick();
    await project.servTypeInput('Doors');
    await project.propertyInput('Eurest Test Property');
    await project.prioritySelect('Normal');
    await project.locationInput('Ternopil');
    await project.descriptionInput(descriptionWO);
    await project.createWOclick();
    await hoverMenus.hoverArrowMenuClick();
    await hoverMenus.woQueueClick();
    await scrollDownToElement(project.elemByText(descriptionWO));
    expect(
      await elementIsDisplay(project.elemByText(descriptionWO), 7000),
    ).toBeTruthy();
  });

  it('Test 12. Pickup WO From Queue', async () => {
    const technician = "Vitalii Technician Lead";
    await project.clickElemByText(descriptionWO);
    await project.pickUpWO();
      expect(
          await elementIsDisplay(project.technicianIsAssign(technician), 5000),
      ).toBeTruthy();
  });

  it('Test 13. Edit Response Team', async () => {
    const technician = "Account For Tests";
    await project.editRespTeam(technician);
      expect(
          await elementIsDisplay(project.technicianIsAssign(technician), 5000),
      ).toBeTruthy();
  });

  it('Test 14. Blocked WO', async () => {
    const texUpdate = 'Cancel WO From Mobile Auto Test';
    await project.startWOclick();
    await project.statusFromTo('IN PROCESS', 'CANCEL ORDER');
    await project.statusUpdateText(texUpdate);
    await project.cancelOrderPress();
    expect(
        await elementIsDisplay(project.woStatus('BLOCKED'), 3000),
    ).toBeTruthy();
  });
});



