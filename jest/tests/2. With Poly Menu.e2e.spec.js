import { loginMobileApp, techLead } from './Page-objects/Login.js';
import { elementGetText } from './Helpers/Functions';

import * as polyMenu from './Page-objects/PolyMenu';

describe('With Poly Menu', function() {
    beforeAll(async () => {
        jest.setTimeout(80000);
        await mobile.setTimeouts(10000);
        await loginMobileApp(techLead);
    });

    it('Test 1. Check Time Record For Today', async () => {
        await polyMenu.timeCardClick();
        expect(await elementGetText(polyMenu.hoursTotal)).toMatch(await polyMenu.getHoursFromDB());
    });

});




