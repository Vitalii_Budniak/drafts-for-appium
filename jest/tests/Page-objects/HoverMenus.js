import * as worker from '../Helpers/Functions'

//==========Selectors================
const hoverArrowMenu = '(//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1])[6]';
const hoverMenuBlockIs = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]';
const polyIcon = '//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup';
const closePolyMenu = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.support.v4.widget.i/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]';
const polyMenuIs = '//*[@text="Poly"]';
const onHold = '//*[@text="On Hold"]';
export const myWO = '//*[@text="My Work Orders"]';
export const woQueue = '//*[@text="WO Queue"]';
const pmQueue = '//*[@text="PM Queue"]';
const createNewWO = '//*[@text="CREATE NEW WO"]';
//==========Selectors================


//==========Actions================
export const hoverArrowMenuClick = async () => {
    await worker.clickElement(hoverArrowMenu);
    if (! await worker.elementIsDisplay(hoverMenuBlockIs)){
        await hoverArrowMenuClick();
    }
};

export const polyIconClick = async () => {
    await worker.clickElement(polyIcon);
    if (! await worker.elementIsDisplay(polyMenuIs)){
        await polyIconClick();
    }
};

export const polyMenuCloseClick = async () => {
    await worker.clickElement(closePolyMenu);
};

export const createNewWOClick = async () => {
    await worker.clickElement(createNewWO);
};

export const onHoldClick = async () => {
    await worker.clickElement(onHold);
};

export const myWOClick = async () => {
    await worker.clickElement(myWO);
};

export const woQueueClick = async () => {
    await worker.clickElement(woQueue);
};

export const pmQueueClick = async () => {
    await worker.clickElement(pmQueue);
};

export const ferfreshMyWO = async () => {
    await hoverArrowMenuClick();
    await woQueueClick();
    await hoverArrowMenuClick();
    await myWOClick();
};

//==========Actions================
