import * as worker from '../Helpers/Functions';

//==========Selectors================
const firstProject = '//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[1]';
const firstActiveProject = '(//*[@text="ACTIVE"])[1]';
export const technName = '(//android.widget.TextView[@text="Response Team"]/following-sibling::android.widget.TextView)[1]';
const assets = '//android.widget.TextView[@text="ASSETS"]';
const servType = '(//android.widget.TextView[@text="Service Type*"]//following-sibling::android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText)[1]';
const property = '(//android.widget.TextView[@text="Property*"]//following-sibling::android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText)[1]';
const priority = '(//android.widget.TextView[@text="Priority*"]//following-sibling::android.view.ViewGroup)[1]';
const location = '(//android.widget.TextView[@text="Location"]//following-sibling::android.view.ViewGroup/android.widget.EditText)[1]';
const description = '(//android.widget.TextView[@text="Description*"]//following-sibling::android.view.ViewGroup/android.widget.EditText)[1]';
const assign = '(//android.widget.TextView[@text="Assignee*"]//following-sibling::android.view.ViewGroup)[1]';
const asiignToMe = '//*[@text="Assign to me"]';
const creteWO = '//*[@text="CREATE WORK ORDER"]';
const startWO = '//*[@text="START WORK ORDER"]';
export const statusInProcess = '(//*[@text="IN PROCESS"])[1]';
const updateButtonSend = '//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup';
const responseTeamEdit = '//android.widget.TextView[@text="Response Team"]/following-sibling::android.view.ViewGroup[1]';
//==========Dynamic selectors================
export const elemByText = text => `//*[@text="${text}"]`;
export const timeCheck= time => `//*[@text="HOURS"]/preceding-sibling::android.widget.TextView[@text="${time}"]`;
export const totalCheck = total => `//*[@text="Total"]/following-sibling::android.widget.TextView[@text="$${total}"]`;
export const materialsRowCheck = (desc, quantityPrice, total ) => `//android.view.ViewGroup/child::android.widget.TextView[@text="${desc}"]/following-sibling::android.widget.TextView[@text="${quantityPrice}"]/following-sibling::android.widget.TextView[@text="${total}"]`;
export const assetRowCheck = assetNumb =>`//android.widget.ScrollView//android.view.ViewGroup/android.widget.TextView[@text="${assetNumb}"]`;
export const woStatus = currentStatusWO => `//android.widget.ScrollView//android.widget.TextView[@text="${currentStatusWO}"]`;
export const technicianIsAssign = technName => `//android.widget.TextView[@text="Response Team"]//following-sibling::android.widget.TextView[@text="${technName}"]`;
//==========Dynamic selectors================

//==========Selectors================

//==========Actions================

export const clickElemByText = async (text) => {
    await worker.clickElement(`(//*[@text="${text}"])[1]`);
};

export const firstProjectClick = async () => {
    await worker.clickElement(firstProject);
};

export const firstActiveProjectClick = async () => {
    if (await worker.elementIsDisplay(firstActiveProject, 1000)){
        await worker.clickElement(firstActiveProject);
    }
    else {
        await worker.scrollDown();
        await firstActiveProjectClick();
    }
};

export const assetsClick = async () => {
    await worker.clickElement(assets);
};

export const servTypeInput = async (text) => {
    let str = text.slice(0,text.length-1);
    await worker.clickElement(servType);
    await worker.sendKeys(servType, str);
    await worker.clickElement(`//*[@text="${text}"]`)
};

export const propertyInput = async (text) => {
    let str = text.slice(0,text.length-1);
    await worker.clickElement(property);
    await worker.sendKeys(property, str);
    await worker.clickElement(`//*[@text="${text}"]`);
};

export const prioritySelect = async (text) => {
    await worker.clickElement(priority);
    await worker.clickElement(`//*[@text="${text}"]`)
};

export const locationInput = async (text) => {
    await worker.sendKeys(location, text);
};

export const descriptionInput = async (text) => {
    await worker.sendKeys(description, text);
};

export const assignToMeSelect = async () => {
    await worker.clickElement(assign);
    await worker.clickElement(asiignToMe)
};

export const createWOclick = async () => {
    await worker.clickElement(creteWO);
};

export const startWOclick = async () => {
    await worker.clickElement(startWO);
};

export const updatesEnter = async (updText) => {
    await worker.clickElement(elemByText("UPDATES"));
    await worker.sendKeys(elemByText("Enter on Update"), updText);
    await worker.clickElement(updateButtonSend);
};

export const timeEnter = async () => {
    const hours = '//*[@text="Add Time"]/parent::android.view.ViewGroup//android.widget.ScrollView[1]/android.view.ViewGroup/android.widget.TextView[3]';
    const minutes = '//*[@text="Add Time"]/parent::android.view.ViewGroup//android.widget.ScrollView[2]/android.view.ViewGroup/android.widget.TextView[3]';
    await worker.scrollDownToElement(elemByText("TIME"));
    await worker.clickElement(elemByText("TIME"));
    await worker.clickElement(elemByText("ADD TIME"));
    //wait untill time modal window will be already opened
    await worker.elementIsDisplay(elemByText("Add Time"));
    //

    //define size and X and Y location for calculating drag and drop action in pixels
    const hourCoords = await worker.elementCoords(hours);
    const hourSize = await worker.elementSize(hours);
    const minitesCoords = await worker.elementCoords(minutes);
    const minutesSize = await worker.elementSize(minutes);
    let x1 = await hourCoords.x;
    let y1 = await hourCoords.y;
    let x2 = await x1;
    let y2 = (await hourCoords.y) - (await hourSize.height);
    //

    //do shifting for 1 position
    await worker.pressAndMove(x1, y1, x2, y2);
    //

    //same with minutes
    x1 = await minitesCoords.x;
    y1 = await minitesCoords.y;
    x2 = await x1;
    y2 = (await minitesCoords.y) - (await minutesSize.height);
    await worker.pressAndMove(x1, y1, x2, y2);
    //
    await worker.clickElement(elemByText("ADD TIME"))
};

export const addMaterials = async () => {
    await worker.scrollDownToElement(elemByText("MATERIALS"));
    await worker.clickElement(elemByText("MATERIALS"));
    await worker.clickElement(elemByText("ADD MATERIAL"));
    //wait until time modal window will be already opened
    await worker.elementIsDisplay(elemByText("Add Material"));
};

export const addMeterialsData = async (descr, quantity, price) => {
    await worker.sendKeys(elemByText("Enter description"), descr);
    await worker.sendKeys(elemByText("Enter quantity"), quantity);
    await worker.sendKeys(elemByText("Enter price"), price);
};

export const assetsAdd = async (assetNumber) => {
    const  assetNumberFromDropDown = `//android.widget.TextView[@text="${assetNumber}"]/parent::android.view.ViewGroup`;
    const  asseSearchIcon = '//*[@text=""]';
    await worker.scrollDownToElement(elemByText("ASSETS"));
    await worker.clickElement(elemByText("ASSETS"));
    await worker.clickElement(elemByText("ADD ASSET"));
    await worker.elementIsDisplay(elemByText("EQUIPMENT TYPE"));
    await worker.clickElement(elemByText("EQUIPMENT TYPE"));
    await worker.sendKeys(elemByText("Search"), assetNumber);
    await worker.clickWhileElemenIsDisplay(assetNumberFromDropDown, asseSearchIcon)
};

export const statusFromTo = async (from, to) => {
    await worker.clickElement(`//android.support.v4.widget.i/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup//android.widget.TextView[@text="${from}"]`);
    await worker.clickElement(`//android.widget.TextView[@text="${to}"]`);
};

export const onHoldFromTo = async (from, to) => {
    await worker.clickElement(`//android.widget.EditText[@text="${from}"]`);
    await worker.clickElement(`//android.widget.TextView[@text="${to}"]`);
};

export const statusUpdateText = async (text) => {
    await worker.sendKeys('//android.widget.EditText[@text="Enter to Update"]', text)
};

export const holdPress = async () => {
    await worker.clickElement(elemByText("HOLD"));
};

export const cancelOrderPress = async () => {
    await worker.clickElement(elemByText("CANCEL ORDER"));
};

export const inProcessPress = async () => {
    await worker.clickElement(elemByText("IN PROCESS"));
};

export const completedPress = async () => {
    await worker.clickElement(elemByText("COMPLETED"));
};

export const pickUpWO = async () => {
    await clickElemByText("PICK UP WORK ORDER");
};

export const editRespTeam = async (technician) => {
    await worker.clickElement(responseTeamEdit);
    await worker.sendKeys("//android.widget.EditText[@text='Search by Name']", technician);
    await worker.clickVisibleElement(`//android.widget.TextView[@text="${technician}"]`);
    await worker.clickElement("//android.widget.TextView[@text='SAVE']");
};


//==========Actions================

