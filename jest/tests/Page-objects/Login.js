import * as worker from '../Helpers/Functions'
import { elemByText } from './Projects'
const secrets = require('../../../../secrets');

//==========Creds To Login App================
export const techLead = secrets.accountsMobile.techLeadLogin;
//==========Creds To Login App================

//==========Actions================

export const loginMobileApp = async (user) => {
    await worker.sendKeys(elemByText('Enter your email address'), user);
    await worker.sendKeys(elemByText('Enter your password'), secrets.accountsMobile.password);
    await worker.clickElement(elemByText('GET STARTED'));
};
//==========Actions================

