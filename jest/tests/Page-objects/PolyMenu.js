import * as worker from '../Helpers/Functions'

const { MongoClient } = require('mongodb');
const dateFns = require("date-fns");

//Selectors
const polyIcon = '//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup';
export const hoursTotal = "//*[@text='HOURS | TOTAL']/preceding-sibling::android.widget.TextView";
export const elemByText = text => `//*[@text="${text}"]`;
//Selectors

export const timeCardClick = async () => {
    await worker.clickElement(polyIcon);
    await worker.clickElement(elemByText("MY TIME CARD"));
};

const formatDate = daysShift => {
    const date = dateFns.addDays(new Date(), daysShift);
    const mounth = dateFns.format(date, "MM");
    const day = dateFns.format(date, "DD");
    const year = dateFns.format(date, "YYYY");
    return `${year}-${mounth}-${day}T21:59:59.999Z`;
};

export const getHoursFromDB = async () => {
    const startDate = formatDate(-1);
    const endDate = formatDate(0);

    const clientInstance = new MongoClient('mongodb://localhost:27017/');
    const mongoConnection = await clientInstance.connect();
    const db = await mongoConnection.db('meteor');
    const totalHours = new Promise((resolve, reject) => {
        db.collection("time-logs")
            .aggregate(
                {$match: {assignedTo: "5cab1dd12691857b41bc6363", startDate:{$gte:new Date(startDate)}, endDate:{$lt:new Date(endDate)} }},
                {$group: {_id: "res", duration: {$sum: "$duration"}}},
                {$out: "result"}
            )
            .toArray()
            .then(data => {
                if (data.length>0) {
                    const milliseconds = data[0].duration;
                    const hours = milliseconds / 1000 / 60 / 60;
                    let floatFormat = (hours+0.001).toString().slice(0,4);
                    resolve(floatFormat);
                }
                else {
                    resolve("0.00");
                }
            });
    });
    await totalHours;
    await clientInstance.close();
    return totalHours
};


