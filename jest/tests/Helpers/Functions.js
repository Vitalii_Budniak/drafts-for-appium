export const elementGetText = async selector => {
  const elem = await mobile.$(selector);
  const text = await elem.getText();
  console.log(await text);
  return text;
};

export const elementIsDisplay = async (selector, timeuot) => {
  let mlsToWait = 5000;
  if (typeof timeuot !== 'undefined') {
    mlsToWait = timeuot;
  }
  try {
    await mobile.setTimeouts(mlsToWait);
    const elem = await mobile.$(selector);
    const isDispl = await elem.isDisplayed();
    await mobile.setTimeouts(10000);
    return isDispl;
  } catch (err) {
    console.error(`Error happens with element ${selector}. *Error:* ${err}`);
    return false;
  }
};

export const clickElement = async selector => {
  const elem = await mobile.$(selector);
  await elem.click();
};

export const clickVisibleElement = async selector => {
  if (await elementIsDisplay(selector)) {
    await clickElement(selector);
  } else {
    console.warn(`Element can not click ${selector}`);
  }
};

export const scrollDown = async () => {
  await mobile.pause(500);
  await mobile.touchAction([
    { action: 'press', x: 2, y: 1500 },
    { action: 'wait', ms: 1000 },
    { action: 'moveTo', x: 2, y: 250 },
    'release',
  ]);
};

export const scrollUp = async () => {
  await mobile.pause(500);
  await mobile.touchAction([
    { action: 'press', x: 2, y: 250 },
    { action: 'wait', ms: 1000 },
    { action: 'moveTo', x: 2, y: 1500 },
    'release',
  ]);
};

/*
export const scrollDownToElement = async (selector) => {
    if (! await elementIsDisplay (selector)){
        await scrollDown();
        await scrollDownToElement (selector);
    }
 };
*/

export const scrollDownToElement = async selector => {
  if (!(await elementIsDisplay(selector, 500))) {
    do {
      await scrollDown();
    } while (!(await elementIsDisplay(selector, 1500)));
  }
};

export const clickWhileElemenIsDisplay = async function(
  elementForClick,
  elemIsDsiplay,
) {
  await clickElement(elementForClick);
  await mobile.pause(1000);
  if (await elementIsDisplay(elemIsDsiplay)) {
    await clickWhileElemenIsDisplay(elementForClick, elemIsDsiplay);
  }
};

export const sendKeys = async (selector, key) => {
  const elem = await mobile.$(selector);
  await elem.setValue(key);
};

export const elementCoords = async selector => {
  const elem = await mobile.$(selector);
  return elem.getLocation();
};

export const elementSize = async selector => {
  const elem = await mobile.$(selector);
  return elem.getSize();
};

export const pressAndMove = async (x1, y1, x2, y2) => {
  await mobile.touchAction([
    { action: 'press', x: x1, y: y1 },
    { action: 'wait', ms: 1000 },
    { action: 'moveTo', x: x2, y: y2 },
    'release',
  ]);
};

export const backButton = async () => {
  try {
    await mobile.back();
  } catch (er) {
    await mobile.pause(1000);
    await mobile.back();
  }
};
