const firstNamesSample = require('./source-data/first-name');
const lastNamesSample = require('./source-data/last-name');
const loremWordsSource = require('./source-data/lorem-words');
const companyFirstPart = require('./source-data/adjective');
const companySecondPart = require('./source-data/bs-adjective');
const commerceDapertment = require('./source-data/commerce-department');
const jobsLevelNameDescr = require('./source-data/jobs-names');
const productNameData = require('./source-data/product-name');
const cities = require('./source-data/cities-NY');

// from 0 to (max - 1)

const randomizeString = seedStr => (length) => {
  let random = '';
  for (let i = 0; i < length; i += 1) {
    random += seedStr.charAt(Math.floor(Math.random() * seedStr.length));
  }
  return random;
};

export const getRandomAlphabeticalString = randomizeString(
  'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
);

export const getRandomAlphaNumericalString = randomizeString(
  'abcdefghijklmnopqrstuvwxyz1234567890',
);

export const getRandomNumber = max =>
  Math.floor(Math.random() * max * 10) % max;

export const getRandomArrayValue = arr => arr[getRandomNumber(arr.length)];

export const firstName = () => getRandomArrayValue(firstNamesSample);

export const lastName = () => getRandomArrayValue(lastNamesSample);

export const getRandomFullName = function (array1, array2) {
  return `${getRandomArrayValue(array1)} ${getRandomArrayValue(array2)}`;
};

export const getRandomEmail = () => {
  const fullName = getRandomFullName(firstNamesSample, lastNamesSample);
  const escapedName = fullName.toLowerCase().replace(/\s/g, '.');
  return `${escapedName}_${getRandomNumber(2000)}@random.com`;
};

export const phone = () => {
  let numbers = '1';
  do {
    numbers += getRandomNumber(9);
  } while (numbers.length <= 9);
  return numbers;
};

export const loremWords = () => {
  let words = '';
  for (let i = 0; i < 3; i += 1) {
    words += `${getRandomArrayValue(loremWordsSource)}`;
  }
  return words;
};

export const companyName = () => {
  const one = getRandomArrayValue(companyFirstPart);
  const two = getRandomArrayValue(companySecondPart);
  const three = getRandomArrayValue(firstNamesSample);
  return `${one} ${two} ${three}`;
};

export const commerceDepartment = () => getRandomArrayValue(commerceDapertment);

export const jobsName = () => getRandomArrayValue(jobsLevelNameDescr.job);

export const jobsDescription = () =>
  `${getRandomArrayValue(jobsLevelNameDescr.descriptor)} ${getRandomArrayValue(
    jobsLevelNameDescr.level,
  )}`;

export const jobsType = () => getRandomArrayValue(jobsLevelNameDescr.level);

export const productName = () =>
  `${getRandomArrayValue(productNameData.adjective)} ${getRandomArrayValue(
    productNameData.material,
  )} ${getRandomArrayValue(productNameData.product)}`;

export const cityNY = () => getRandomArrayValue(cities);
