const path = require('path');

const CUSTOM_REPORTER_PATH = path.resolve('../reportCommons');

const { finalizeReport } = require(CUSTOM_REPORTER_PATH);

module.exports = async function () {
  await finalizeReport();
};
