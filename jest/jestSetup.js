const path = require('path');

const CUSTOM_REPORTER_PATH = path.resolve('../reportCommons');

const {
  makeTestResultScreenshot,
  writeTestCaseStatus,
} = require(CUSTOM_REPORTER_PATH);

jasmine.getEnv().addReporter({
  specStarted: (result) => {
    jasmine.currentTest = result;
  },
  specDone: async (result) => {
    jasmine.currentTest = result;
    await writeTestCaseStatus(result);
  },
});

afterEach(async () => {
  await makeTestResultScreenshot(jasmine.currentTest);
});
